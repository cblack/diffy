QtApplication {
	files: ["src/*.cpp", "src/*.h"]
	cpp.includePaths: ["src"]
	cpp.cxxLanguageVersion: "c++17"

	Depends { name: "Qt"; submodules: ["gui", "quick", "quickcontrols2"] }
}
