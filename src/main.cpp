#include <QGuiApplication>
#include <QDebug>

#include <variant>
#include <optional>
#include <type_traits>

#include <QQmlComponent>
#include <QQmlEngine>
#include <QQuickWindow>
#include <QTimer>
#include <QQuickItem>

struct Element {
    QString kind;
    QString key;

    QMap<QString, QVariant> attributes;

    QList<Element> children;
};

struct CreateChild;
struct ModifyChild;
struct RemoveChild;
struct SetAttributes;
struct UnsetAttributes;

using MutationAction = std::variant<CreateChild, ModifyChild, RemoveChild, SetAttributes, UnsetAttributes>;

struct RemoveChild {
    QString key;
};
struct CreateChild {
    Element elm;
    QString afterKey;
};
struct SetAttributes {
    QList<QPair<QString, QVariant>> attrs;
};
struct UnsetAttributes {
    QList<QString> attrs;
};
struct ModifyChild {
    QString key;
    QList<MutationAction> mutations;
};

template<class> inline constexpr bool always_false_v = false;

QDebug operator<< (
      QDebug& dbg
    , const Element& elm
    )
{
    QDebugStateSaver saver(dbg);

    dbg.nospace() << "Element(" << elm.kind << ", " << elm.attributes << ")";

    return dbg;
}

QDebug operator<< (
      QDebug& dbg
    , const MutationAction& mut
    )
{
    QDebugStateSaver saver(dbg);

    std::visit([dbg](auto&& arg) mutable {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, RemoveChild>)
            dbg.nospace() << "RemoveChild(key=" << arg.key << ")";
        else if constexpr (std::is_same_v<T, CreateChild>)
            dbg.nospace() << "CreateChild(after=" << arg.afterKey << ", " << arg.elm << ")";
        else if constexpr (std::is_same_v<T, SetAttributes>)
            dbg.nospace() << "SetAttributes(set=" << arg.attrs << ")";
        else if constexpr (std::is_same_v<T, UnsetAttributes>)
            dbg.nospace() << "UnsetAttributes(set=" << arg.attrs << ")";
        else if constexpr (std::is_same_v<T, ModifyChild>)
            dbg.nospace() << "ModifyChild(key=" << arg.key << ", mutations=" << arg.mutations << ")";
        else 
            static_assert(always_false_v<T>, "non-exhaustive visitor!");
    }, mut);

    return dbg;
}

QList<MutationAction> mutation (
      const std::optional<Element>& from
    , const std::optional<Element>& to
    )
{
    if (from && to) {
        QList<QPair<QString, QVariant>> setAttributes;
        QList<QString> unsetAttributes;

        for (const auto& key : to->attributes.keys()) {
            setAttributes << qMakePair(key, to->attributes[key]);
        }

        for (const auto& key : from->attributes.keys()) {
            if (!to->attributes.contains(key)) {
                unsetAttributes << key;
            }
        }

        auto contains = [](auto foo, auto in) {
            for (const auto& it : in) {
                if (it.key == foo) {
                    return true;
                }
            }
            return false;
        };
        auto find = [](auto foo, auto in) {
            for (const auto& it : in) {
                if (it.key == foo) {
                    return std::optional<Element>(it);
                }
            }
            return std::optional<Element>{};
        };

        QList<MutationAction> muts = {SetAttributes { .attrs = setAttributes }, UnsetAttributes { .attrs = unsetAttributes }};

        for (const auto& it : from->children) {
            if (!contains(it.key, to->children)) {
                muts << RemoveChild { .key = it.key };
            }
        }

        QString prev;

        for (const auto& it : to->children) {
            if (contains(it.key, from->children)) {
                muts << ModifyChild {
                    .key = it.key,
                    .mutations = mutation(find(it.key, from->children), it)
                };
            } else {
                muts << CreateChild { .elm = it, .afterKey = prev };
            }

            prev = it.key;
        }

        return muts;
    } else if (!from && to) {
        Q_UNREACHABLE();
    } else if (from && !to) {
        Q_UNREACHABLE();
    } else if (!from && !from) {
        Q_UNREACHABLE();
    }

    Q_UNREACHABLE();
}

QObject* locate(QObject* in, const QString& name)
{
    if (in->objectName() == name)
        return in;

    if (auto window = qobject_cast<QQuickWindow*>(in)) {
        in = window->contentItem();
    }

    if (auto item = qobject_cast<QQuickItem*>(in)) {
        for (QQuickItem* item : item->childItems()) {
            auto child = locate(item, name);
            if (child)
                return child;
        }
    }

    auto child = in->findChild<QObject*>(name);
    if (child)
        return child;

    return nullptr;
}

void printObjectRecursively(QObject* obj, int level = 0)
{
    qWarning().noquote() << QString("  ").repeated(level) << obj;
    for (QObject* child : obj->children()) {
        printObjectRecursively(child, level + 1);
    }

    if (auto item = qobject_cast<QQuickItem*>(obj)) {
        qWarning().noquote() << QString("  ").repeated(level) << "<items>";
        for (QQuickItem* item : item->childItems()) {
            printObjectRecursively(item, level + 1);
        }
        qWarning().noquote() << QString("  ").repeated(level) << "</items>";
    }
}

struct Applicator
{

private:

    QObject* _make(QObject* parent, const Element& elm)
    {
        const auto& constr = objects[elm.kind];
        auto obj = constr(parent);
        if (parent) {
            obj->setObjectName(parent->objectName() + ":" + elm.key);
        } else {
            obj->setObjectName(elm.key);
        }

        for (const auto& it : elm.attributes.keys()) {
            obj->setProperty(it.toLocal8Bit(), elm.attributes[it]);
        }
        for (const auto& child : elm.children) {
            _make(obj, child);
        }

        return obj;
    }
    void _apply(QObject* obj, const QList<MutationAction>& muts)
    {
        Q_ASSERT(obj != nullptr);
        for (const auto& mut : muts) {
            std::visit([=](auto&& change) mutable {
                using T = std::decay_t<decltype(change)>;
                if constexpr (std::is_same_v<T, RemoveChild>) {
                    auto it = obj->findChild<QObject*>(change.key, Qt::FindDirectChildrenOnly);
                    it->deleteLater();
                } else if constexpr (std::is_same_v<T, CreateChild>) {
                    _make(obj, change.elm);
                } else if constexpr (std::is_same_v<T, SetAttributes>) {
                    for (const auto& it : change.attrs) {
                        obj->setProperty(it.first.toLocal8Bit(), it.second);
                    }
                } else if constexpr (std::is_same_v<T, UnsetAttributes>) {
                    for (const auto& it : change.attrs) {
                        obj->setProperty(it.toLocal8Bit(), QVariant());
                    }
                } else if constexpr (std::is_same_v<T, ModifyChild>) {
                    auto it = locate(obj, obj->objectName() + ":" + change.key);
                    Q_ASSERT(it != nullptr);
                    _apply(it, change.mutations);
                } else static_assert(always_false_v<T>, "non-exhaustive visitor!");
            }, mut);
        }
    }

public:

    QObject* root = nullptr;
    QMap<QString,std::function<QObject*(QObject*)>> objects;
    Element currentElm;

    void construct(const Element& element) {
        root->deleteLater();
        root = nullptr;

        root = _make(nullptr, element);
        currentElm = element;
    };
    void diffTo(const Element& newElm) {
        auto diff = mutation(currentElm, newElm);

        _apply(root, diff);
    }

};

class Component
{
    QSharedPointer<QQmlComponent> comp;

public:
    Component(QQmlEngine* eng, const QString& data)
    {
        comp.reset(new QQmlComponent(eng));
        comp->setData(data.toUtf8(), QUrl());
        while (!comp->isReady() && !comp->isError()) {
            QCoreApplication::processEvents();
        }
        if (comp->isError()) {
            qWarning() << comp->errorString();
        }
    }
    ~Component()
    {
    }

    QObject* operator()(QObject* it)
    {
        auto ctx = qmlContext(it);
        if (!ctx) {
            ctx = comp->engine()->rootContext();
        }
        auto obj = comp->create(ctx);
        if (!obj) {
            qWarning() << comp->errors();
        }
        auto child = qobject_cast<QQuickItem*>(obj);
        auto parent = qobject_cast<QQuickItem*>(it);
        auto parentWindow = qobject_cast<QQuickWindow*>(it);
        if (child && parent) {
            child->setParentItem(parent);
        }
        if (child && parentWindow) {
            child->setParentItem(parentWindow->contentItem());
        }
        return obj;
    }
};

auto component(QQmlEngine* eng, const QString& data) -> std::function<QObject*(QObject*)>
{
    return Component(eng, data);
};

int main(int argc, char* argv[]) {
    const auto tree1 = Element {
        .kind = "window",
        .key = "foo",
        .attributes = {{"color", QColor(Qt::red)}},

        .children = {
            Element {
                .kind = "button",
                .key = "foo",
                .attributes = {{"text", "loje"}},
            }
        }
    };
    const auto tree2 = Element {
        .kind = "window",
        .key = "foo",
        .attributes = {{"color", QColor(Qt::green)}},

        .children = {
            Element {
                .kind = "button",
                .key = "foo",
                .attributes = {{"text", "laso"}},
            }
        }
    };

    QGuiApplication app(argc, argv);

    QQmlEngine eng;

    Applicator applicator;
    applicator.objects["window"] = [](QObject* parent) -> QObject* {
        Q_UNUSED(parent)

        return new QQuickWindow();
    };
    applicator.objects["button"] = component(&eng, "import QtQuick.Controls 2.15 as QQC2; QQC2.Button {}");

    applicator.construct(tree1);
    qobject_cast<QQuickWindow*>(applicator.root)->show();

    QTimer::singleShot(1000, [=]() mutable {
        applicator.diffTo(tree2);
    });

    return app.exec();
}